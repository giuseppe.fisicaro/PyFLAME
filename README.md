# Manual


## About ##
PyFLAME introduces an automated workflow for training neural network interatomic potentials (NNP) with [FLAME](https://gitlab.com/flame-code/FLAME). 

## Dependencies ##
**Note:** Python 3.6 or later is required. 

[FLAME](https://gitlab.com/flame-code/FLAME) (the latest version of FLAME compatible with Python 3 is required)

[atomate](https://atomate.org/installation.html)  is  used heavily in this project and needs to be connected to [MongoDB](https://www.mongodb.com/).

[PyXtal](https://github.com/qzhu2017/PyXtal) 

[CRYSPNet](https://github.com/AuroraLHT/cryspnet)


## Download and installation ##


The source code can be downloaded by

git clone https://gitlab.com/flame-code/PyFLAME.git


There is no installation command. It is required that the PyFLAME directory be in the python path (PYTHONPATH).

## Usage ##

The directory structure of PyFLAME directory is as follows:

PyFLAME <br>
&nbsp;&nbsp;|--- config.py <br>
&nbsp;&nbsp;|--- examples <br>
&nbsp;&nbsp;|--- flame <br>
&nbsp;&nbsp;|--- main.py <br>
&nbsp;&nbsp;|--- OUTPUT <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|--- additional_structures <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|--- input.yaml <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|--- restart.yaml <br>
&nbsp;&nbsp;|--- structure <br>
&nbsp;&nbsp;|--- vasp <br>
&nbsp;&nbsp;|--- workflows

The default output directory is "OUTPUT" which can be modified by the user. It is noted that the output directory is not created by the code. The user can create a different directory for the output, add its path to **config.py**, and copy **input.yaml**, **restart.yaml**, and "additional_structures directory" into the created directory (see below for more details). 

It is necessary to modify **config.py**, **input.yaml**, and **restart.yaml** files before running the script.


**config.py** contains the absolute path to the config directory of atomate (CONFIG_DIR), current directory (CWD), and the output directory (OUTPUT_FOLDER).

The data that should be provided in **input.yaml** is as follows:

|Key                |Description|
|-|-|
|Composition              |One of these keywords should be provided. <br> MP_ID: specifies the composition by providing an mp-id <br> species: specifies the composition by providing species, i. e. [element, oxidation state] <br> chemical_formula: specifies the composition(s) by chemical formula(s)|
|fixed_stoichiometry| Determines if the training data is limited to a specific stoichiometry. It can be "True" if MP_ID is provided.|
|min_atom_bulk      | The minimum number of atoms in bulk structures (not smaller than 4).|
|max_atom_bulk      | The maximum number of atoms in bulk structures.|
|crystal_structure_prediction_method|A list of methods should be provided. <br> 0: No crystal structure prediction. Random structure generation by PyXtal (in combination with chemical_formula or MP_ID) <br> 1: Ionic-sbstitution crystal structure prediction (in combination with MP_ID or species) <br> 2: CRYSPNet crystal structure prediction (in combination with chemical_formula or MP_ID)|
|threshold|Threshold for the crystal structure prediction: the smaller the value, the more atomic configurations|
|ionic_substitution| User specified ion substitution. A "percentage" of the "first_specie" is replaced by the "second_specie". |
|percentage| |
|first_specie| |
|second_specie| |
|max_number_of_perturbed_structures| Maximum number of generated perturbed bulk structures. Up to 14 stressed structures for each atomic configuration will be added.|
|cluster_calculation| If cluster structures should be included in the training.|
|min_atom_cluster|Minimum number of atoms in clusters|
|max_atom_cluster|Maximum number of atoms in clusters|
|additional_structures|If additional atomic structures are provided by user. If "True", a json file containing the dict representation of the [Pymatgen Structure/Molecule](https://pymatgen.org/pymatgen.core.structure.html) in the "additional_structures" directory will be read.|
|min_distance_prefactor|The allowed minimum distance between atoms in a structure is 2*the smallest covalent radius of the constitutional elements. The user can tune the  allowed value by this prefactor. |
|**FLAME related parameters:**||
|n_atom_in_rcut| To determine the radius cut for symmetry functions. "n" indicates number of atoms inside the "rcut" radius.|
|method|behler|
|number_of_nodes|Number of nodes in each hidden layer of the NN.|
|number_of_epoch| Number of epoch in model training.|
|minhocao_temp| A list of temperatures for different steps of minima hopping calculations|
|minhocao_max_temp| A list of the maximum allowed temperature for minima hopping calculations|
|minhocao_n_min| Maximum number of structures to be found by minima hopping|
|dtol_prefactor| Prefactor for structure diversity check. The larger the value is, the more structures are considered similar.|

<br>
The step from which the code will (re)start is specified in **restart.yaml**. The script keeps track of its steps. A detailed log file will be written in the output directory (the default name is pyflame.log). If a failure occurs and the script cannot advance, the user can restart the script from the last successfully accomplished step. It is noted that the user can always restart the script from a previous step with different parameters.

The following parameters can be specified in **restart.yaml**:

|Key                |Description|
|-|-|
|re-start|Specifies the step the script will (re)start.<br>0: runs jobs in the Launchpad <br>1: crystal structure prediction/random bulk structure generation <br>2: add and run jobs for bulk structure optimization <br>3: data collection from bulk structure calculations <br>4: create perturbed structures <br>5: add and run perturbed bulk structure optimization and/or additional structures provided by user <br>6: data collection from step 5 <br>7: FLAME calculations|
|stop_after_step|The script stops after the end of this step.|
|FLAME_step_number|Specifies the step number for the training cycle, if "re-start: 7".|
|FLAME_step_name|Specifies the step name of the model training process.<br>train: training the model<br>minhocao: minima hopping for bulk structures<br>minhopp: minima hopping for clusters<br>divcheck: structure diversity check<br>VASP_NSW0: DFT single point energy calculation<br> VASP_collecting_data: collecting and preparing data for the next cycle of training|
|FLAME_training_cycles|Specifies number of training cycles.|

When the above-mentioned files are ready, the script can be executed by running the following command:

**python main.py**

<br>

### Files created by PyFLAME ###
Here we provide a short description of the files and directories created by PyFLAME.

** json files **

Most json files will be read by the code in the later steps and should be kept in the same place that they are created. Here is the list of the most important json files:

|file name|description|
|-|-|
|all_bulk_structures.json| Created in step 1. Contains bulk structures generated by crystal structure prediction or random structure generation.|
|fixed_stoichiometry_bulk_structures.json| Created in step 1, if "fixed_stoichiometry" is True. It contains structures with the specified stoichiometry.|
|cluster_structures.json| Created in step 1, if the "cluster_calculation" keyword is True. |
|relaxed_bulk_structures.json| Created in step 3. It contains relaxed bulk atomic configurations.|
|final_relaxed_bulk_structures.json| Created in step 3. It contains non-duplicated relaxed bulk structures that have low formation energies. These structures are seed configurations for minima hopping.|
|ave_dis.json| Create in step 7. It contains a list of average distances of fingerprints of structures with different number of atoms. This data will be used in the  structure diversity check step.|
|poslow_structures.json| Created in step 7 and contains the structures that will be used as the seed configuration of minima hopping in the next training cycle.|

** FLAME_calculations directory **

The structure of the directory is as follows (n indicates the cycle of training):

FLAME_calculations <br>
&nbsp;&nbsp;|--- aver_dist <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  aver_dist.json <br>
&nbsp;&nbsp;|--- step-0 <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  task_files <br>
&nbsp;&nbsp;|--- step-1 <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  divcheck <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  minhocao <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  minhopp <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  task_files <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  train <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  VASP_run <br>
&nbsp;&nbsp;. <br>
&nbsp;&nbsp;. <br>
&nbsp;&nbsp;. <br>
&nbsp;&nbsp;|--- step-n <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  divcheck <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  minhocao <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  minhopp <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  task_files <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  train <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---  VASP_run <br>

There are 5 (6 if cluster calculations are included) directories inside each step directory. Inside each of these directories, a directory created by atomate to perform corresponding calculations (directories' name starts with block_). The block_* directories inside VASP_run, and minhocao/minhopp could be very large and can safely removed by the user when the corresponding calculations of a step end. 


### Note on job scripts ###
The details of the job script are provided in the config directory of atomate. The number of jobs, number of nodes/cores can be overwritten by modifying PyFLAME/workflows/job_script/job_script.yaml. The user can specify these values for each type of the job listed in PyFLAME/workflows/job_scriptlist_of_jobtypes.yaml.

### Note on VASP files ###
The user can modify VASP input files by modifying files in the PyFLAME/vasp/vasp_files directory.

### Note on the config directory of atomate ###
It is required to add the path of the FLAME executable file to my_fworker.yaml in the config directory of atomate: 
flame_cmd: path_to_the_FLAME_executable_file. It is noted that this file will be modified by PyFLAME to adapt the category of jobs. 

### Examples ###
The directory examples contains the constructed NN pontentials for (Ti, 4)-(O, -2) and (In, 3)-(Cu, 1)-(Se, -2) systesm. The findings of [this work](https://arxiv.org/abs/2102.04085) can be reproduced by these potentials. 

## Citation ##

@article{PyFLAME,<br>
title = "An automated approach for developing neural network interatomic potentials with FLAME",<br>
author = "Mirhosseinia, Hossein and Tahmasbib, Hossein and Kuchanaa, Sai Ram  and Ghasemia, S. Alireza and Kühne, Thomas D.",<br>
journal = "",<br>
volume = "",<br>
pages = "",<br>
year = "2021",<br>
issn = "",<br>
doi = "",<br>
url = "",<br>
}

## How to contribute ##


## License ##
