import io
import os
import sys
import shutil
import re
import time
import json
import yaml
import math
from yaml import Loader 
from itertools import combinations_with_replacement

from pymatgen.core.structure import Structure
from pymatgen.core.periodic_table import Element

from config import *
from structure.core import min_distance, r_cut

def write_p_f_from_list(struct_list, bc, energy_list, force_list, file_path): # structure as dict, boundary condiction, energy in eV
    todump_list = []
    for s_i in range(len(struct_list)):
        todump_dict = dict()
        todump_dict = {'conf':{}}
        lattice = Structure.from_dict(struct_list[s_i]).lattice.matrix
        sites =   Structure.from_dict(struct_list[s_i]).sites
        energy = energy_list[s_i]
        todump_dict['conf']['bc'] = bc
        todump_dict['conf']['cell'] = []
        todump_dict['conf']['cell'].append([float(lattice[0][0]),float(lattice[0][1]),float(lattice[0][2])])
        todump_dict['conf']['cell'].append([float(lattice[1][0]),float(lattice[1][1]),float(lattice[1][2])])
        todump_dict['conf']['cell'].append([float(lattice[2][0]),float(lattice[2][1]),float(lattice[2][2])])
        todump_dict['conf']['coord'] = []
        for i in range(0,len(sites)):
            elements = struct_list[s_i]['sites'][i]['species'][0]['element']
            todump_dict['conf']['coord'].append([float(sites[i].x), float(sites[i].y), float(sites[i].z), elements, 'TTT'])
        todump_dict['conf']['epot'] = energy*0.036749309
        if force_list:
            forces = force_list[s_i]
            todump_dict['conf']['force'] = []
            for i in range(0,len(forces)):
                todump_dict['conf']['force'].append([forces[i][0]*0.01944689673, forces[i][1]*0.01944689673, forces[i][2]*0.01944689673])
        todump_dict['conf']['nat'] = len(sites)
        todump_dict['conf']['units_length'] = 'angstrom'
        todump_list.append(todump_dict)
    with open(file_path, 'w') as f:
        yaml.dump_all(todump_list, f, default_flow_style=None)

def write_SE_ann_FLAME(elmnt_list):
    g02_list = ['0.0010  0.0000  0.0000  0.0000', '0.0100  0.0000  0.0000  0.0000', '0.0200  0.0000  0.0000  0.0000', '0.0350  0.0000  0.0000  0.0000', '0.0600  0.0000  0.0000  0.0000', '0.1000  0.0000  0.0000  0.0000', '0.2000  0.0000  0.0000  0.0000', '0.4000  0.0000  0.0000  0.0000']
    g05_list = ['0.0001  1.0000  1.0000  0.0000  0.0000', '0.0001  1.0000 -1.0000  0.0000  0.0000', '0.0001  2.0000  1.0000  0.0000  0.0000','0.0001  2.0000 -1.0000  0.0000  0.0000','0.0001  4.0000  1.0000  0.0000  0.0000','0.0001  4.0000 -1.0000  0.0000  0.0000','0.0080  1.0000  1.0000  0.0000  0.0000','0.0080  1.0000 -1.0000  0.0000  0.0000','0.0080  2.0000  1.0000  0.0000  0.0000','0.0080  2.0000 -1.0000  0.0000  0.0000','0.0080  4.0000  1.0000  0.0000  0.0000','0.0080  4.0000 -1.0000  0.0000  0.0000','0.0250  1.0000  1.0000  0.0000  0.0000','0.0250  1.0000 -1.0000  0.0000  0.0000','0.0250  2.0000  1.0000  0.0000  0.0000','0.0250  2.0000 -1.0000  0.0000  0.0000','0.0250  4.0000  1.0000  0.0000  0.0000','0.0250  4.0000 -1.0000  0.0000  0.0000']
    rcut = r_cut() * 1.88973 #A to Bohr
    F_number_of_nodes = input_list['number_of_nodes']
    F_ener_ref = 0.0
    F_method = input_list['method']
    combinations_index = list(combinations_with_replacement(range(len(elmnt_list)),2))
    for elmnt in elmnt_list:
        f_name = str(elmnt) + '.ann.input.yaml'
        with open(f_name, 'a') as f:
            f.write('main:'+'\n')
            f.write('    nodes: [{},{}]'.format(F_number_of_nodes,F_number_of_nodes)+'\n')
            f.write('    rcut:        {}'.format(rcut)+'\n')
            f.write('    ener_ref:    {}'.format(F_ener_ref)+'\n')
            f.write('    method:      {}'.format(F_method)+'\n'+'\n')
            f.write('symfunc:'+'\n')
            n = 0
            for g in g02_list:
                for i in range(0,len(elmnt_list)):
                    n = n + 1
                    f.write('    g02_{:03d}: {}    {}'.format(n,g,elmnt_list[i])+'\n')
            n = 0
            for g in g05_list:
                for i in range(0,len(combinations_index)):
                    n = n + 1
                    f.write('    g05_{:03d}: {}    {}    {}'.format(n,g,elmnt_list[combinations_index[i][0]],\
                                                                        elmnt_list[combinations_index[i][1]])+'\n')
