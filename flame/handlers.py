import os 
from custodian.custodian import ErrorHandler
import time 

class FrozenFlameErrorHandler(ErrorHandler):
    
    is_monitor = True

    def __init__(self, output_filename="flame_log.yaml", timeout=10800):

        self.output_filename = output_filename
        self.timeout = timeout

    def check(self):
        """
        Check for error.
        """
        st = os.stat(self.output_filename)
        if time.time() - st.st_mtime > self.timeout:
            return True
        return None

    def correct(self):
        actions = []
        return {"errors": ["Frozen job"], "actions": None}

class FrozenDivcheckErrorHandler(ErrorHandler):

    is_monitor = True

    def __init__(self, output_filename="flame_log.yaml", timeout=21600):

        self.output_filename = output_filename
        self.timeout = timeout

    def check(self):
        """
        Check for error.
        """
        st = os.stat(self.output_filename)
        if time.time() - st.st_mtime > self.timeout:
            return True
        return None

    def correct(self):
        actions = []
        return {"errors": ["Frozen job"], "actions": None}

#temporary
class FrozenMinhocaoErrorHnadler(ErrorHandler):

    is_monitor = True

    def __init__(self, output_filename="flame.err", timeout=14400): # kill minhocao jobs after 4h 

        self.output_filename = output_filename
        self.timeout = timeout

    def check(self):
        """
        Check for error.
        """
        st = os.stat(self.output_filename)
        if time.time() - st.st_mtime > self.timeout:
            return True
        return None

    def correct(self):
        actions = []
        return {"errors": ["Frozen minhocao job"], "actions": None}

