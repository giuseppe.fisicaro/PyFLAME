import os 
import sys
import re 

from fireworks import Firework, PyTask
from flame import *
from flame.run import RunFlameCustodian

__author__ = "Sai Ram Kuchana and Hossein Mirhosseini"
__maintainer__ = "Hossein Mirhosseini"
__email__ = "mirhosse@mail.uni-paderborn.de"


class AverDistFW(Firework):
    def __init__(
       self,
       structure,
       name,
       flame_cmd = ">>flame_cmd<<",
       parents=None,
       **kwargs
    ):
        t = []
        t.append(PyTask(func='flame.aver_dist.write_aver_dist_files', args = [structure]))
        t.append(RunFlameCustodian(flame_cmd = flame_cmd))
        super(AverDistFW, self).__init__(t, parents = parents, name = name, **kwargs)

class TrainFW(Firework):
    def __init__(
       self,
       step_number,
       name,
       flame_cmd = ">>flame_cmd<<",
       parents=None,
       **kwargs
    ):
        t = []
        t.append(PyTask(func='flame.train.write_train_files', args = [step_number]))
        t.append(RunFlameCustodian(flame_cmd = flame_cmd))
        super(TrainFW, self).__init__(t, parents = parents, name = name, **kwargs)

class MinhocaoFW(Firework):
    def __init__(
       self,
       structure,
       step_number,
       name,
       flame_cmd = ">>flame_cmd<<",
       parents=None,
       **kwargs
    ):
        t = []
        t.append(PyTask(func='flame.minhocao.write_minhocao_files', args = [structure, step_number]))
        t.append(RunFlameCustodian(flame_cmd = flame_cmd, job_type = "minhocao"))
        super(MinhocaoFW, self).__init__(t, parents = parents, name = name, **kwargs)

class MinhocaoStoreFW(Firework):
    def __init__(
       self,
       step_number,
       name,
       parents=None,
       **kwargs
    ):  
        t = []
        t.append(PyTask(func='flame.minhocao.store_minhocao_results', args = [step_number]))
        super(MinhocaoStoreFW, self).__init__(t, parents = parents, name = name, **kwargs)


class MinhoppFW(Firework):
    def __init__(
       self,
       structure,
       step_number,
       name,
       flame_cmd = ">>flame_cmd<<",
       parents=None,
       **kwargs
    ):
        t = []
        t.append(PyTask(func='flame.minhopp.write_minhopp_files', args = [structure, step_number]))
        t.append(RunFlameCustodian(flame_cmd = flame_cmd, job_type = "minhopp"))
        super(MinhoppFW, self).__init__(t, parents = parents, name = name, **kwargs)

class MinhoppStoreFW(Firework):
    def __init__(
       self,
       step_number,
       name,
       parents=None,
       **kwargs
    ):
        t = []
        t.append(PyTask(func='flame.minhopp.store_minhopp_results', args = [step_number]))
        super(MinhoppStoreFW, self).__init__(t, parents = parents, name = name, **kwargs)


class DivCheckbFW(Firework):
    def __init__(
       self,
       number_of_atom,
       step_number,
       name,
       flame_cmd = ">>flame_cmd<<",
       parents=None,
       **kwargs
    ):
        t = []
        t.append(PyTask(func='flame.divcheck.write_divcheck_b_files', args = [step_number, number_of_atom]))
        t.append(RunFlameCustodian(flame_cmd = flame_cmd, job_type = "divcheck"))
        t.append(PyTask(func='flame.divcheck.run_pickdiff', args = [step_number, number_of_atom]))
        super(DivCheckbFW, self).__init__(t, parents = parents, name = name, **kwargs)

class DivCheckcFW(Firework):
    def __init__(
       self,
       number_of_atom,
       step_number,
       name,
       flame_cmd = ">>flame_cmd<<",
       parents=None,
       **kwargs
    ):
        t = []
        t.append(PyTask(func='flame.divcheck.write_divcheck_c_files', args = [step_number, number_of_atom]))
        t.append(RunFlameCustodian(flame_cmd = flame_cmd, job_type = "divcheck"))
        t.append(PyTask(func='flame.divcheck.run_pickdiff', args = [step_number, number_of_atom]))
        super(DivCheckcFW, self).__init__(t, parents = parents, name = name, **kwargs)


