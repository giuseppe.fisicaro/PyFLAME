import os
import sys
import shutil
import re
import time
import math
import json
import yaml
from yaml import Loader 
from collections import defaultdict

from pymatgen.core.structure import Structure
from pymatgen.io.vasp import Poscar
from pymatgen.core.periodic_table import Element

from structure.io_db import get_element_list
from structure.core import min_distance
from config import *

import flame.flame_functions.atoms
from flame.flame_functions.ascii import *
from flame.flame_functions.io_yaml import *
from flame.flame_functions.io_bin import *
from flame.flame_functions.vasp import *
from flame.flame_functions.latvec2dproj import *

def write_minhopp_files(struct, step_number):
#element list
    elmnt_list = get_element_list()
#write posinp for minhopp  both vasp and yaml format

    Poscar(struct).write_file('posinp.vasp')
    atoms=poscar_read('posinp.vasp')
    atoms.units_length_io='angstrom'
    atoms.boundcond='free'
    atoms_out=[]
    atoms_out.append(Atoms())
    atoms_out[-1]=copy.copy(atoms)
    write_yaml(atoms_out,'posinp.yaml')
#write flame_in.yaml for minhopp
    with open('flame_in.yaml', 'w') as f:
        f.write('main:'+'\n')
        f.write('    task: minhopp'+'\n')
        f.write('    two_level_geopt: True'+'\n')
        f.write('    types:')
        for elmnt in elmnt_list:
            f.write(' {}'.format(elmnt))
        f.write('\n'+'\n')

        f.write('potential:'+'\n')
        f.write('    potential: ann'+'\n'+'\n')

        f.write('geopt:'+'\n')
        f.write('    method: FIRE'+'\n')
        f.write('    fmaxtol: 2.E-5'+'\n')
        f.write('    alphax: 3.0'+'\n')
        f.write('    lprint: True'+'\n')
        f.write('    print_force: True'+'\n')
        f.write('    dt_start: 1.E-2'+'\n')
        f.write('    dt_max: 1.0'+'\n'+'\n')

        f.write('geopt_prec:'+'\n')
        f.write('    method: SD'+'\n')
        f.write('    fmaxtol: 1.E-3'+'\n')
        f.write('    alphax: 2.0'+'\n'+'\n')

        f.write('minhopp:'+'\n')
        f.write('    nstep: {}'.format(input_list['minhocopp_steps'])+'\n')
        f.write('    nsoften: 20'+'\n')
        f.write('    mdmin: 3'+'\n')
        f.write('    etoler: 1.E-3'+'\n')
        f.write('    nrandoff: 5'+'\n')
        f.write('    eref: -44.325801'+'\n')
        f.write('    npminx: 5000'+'\n')
        f.write('    trajectory: True'+'\n')
        f.write('    print_force: True'+'\n')

#write input for minhopp
    with open('input.minhopp', 'w') as f:
       f.write('             0 number of minima already found'+'\n')
       f.write('   0.01    0.001  0.1      ediff,ekin,dt'+'\n')
#cp ann files from train directory
    number_of_epoch = input_list['number_of_epoch']
    for root, dirs, files in os.walk(os.path.join(Flame_dir,step_number,'train',)):
        for f in files:
            for elmnt in elmnt_list:
                if f == str(elmnt)+'.ann.param.yaml.'+str(number_of_epoch).zfill(5):
                    shutil.copyfile(os.path.join(root,str(elmnt)+'.ann.param.yaml.'+str(number_of_epoch).zfill(5)),\
                                    os.path.join('./',str(elmnt)+'.ann.param.yaml'))


def store_minhopp_results(step_number):
#a dynamicd allowed minimum distance
    min_d = min_distance() * (1.1-(int(re.split('-',step_number)[1])/10))
    if (1.1-(int(re.split('-',step_number)[1])/10)) < 0.5:
        min_d = min_distance() * 0.5
    global all_minhopp_structs
    all_minhopp_structs = defaultdict(list)
    read_poslows(os.path.join(Flame_dir,step_number,'minhopp'), min_d)
    read_trajectories(os.path.join(Flame_dir,step_number,'minhopp'), min_d)
# store data
    todump = defaultdict(list)
    for keys in all_minhopp_structs.keys():
        for values in all_minhopp_structs[keys]:
            if len(todump[keys]) < 10000:
                 todump[keys].append(values)
            else:
                break
    with open(os.path.join(Flame_dir,step_number,'minhopp','minhopp-'+step_number+'.json'), 'w') as f:
        json.dump(all_minhopp_structs, f)

def read_poslows(path, min_d):
    nat_list = []
    with open(os.path.join(bulk_structure_optimization_dir,'ave_epa.dat'), 'r') as f:
        ave_epa = f.readline().strip()
    for root, dirs, files in os.walk(path):
        if 'posinp.yaml' in files:
            if 'poslow.yaml' in files:
                ref_struct = Structure.from_file(os.path.join(root,'posinp.vasp'))
                n_atom = len(ref_struct.sites)
                if n_atom not in nat_list:
                    all_minhopp_structs[n_atom] = []
                    nat_list.append(n_atom)
                atoms_all_yaml=read_yaml(os.path.join(root,'poslow.yaml'))
                for atoms in atoms_all_yaml:
                    conf = atoms2dict(atoms)
                    lattice = conf['conf']['cell']
                    crdnts = []
                    spcs = []
                    for coord in conf['conf']['coord']:
                        crdnts.append([coord[0],coord[1],coord[2]])
                        spcs.append(coord[3])
                    structure=Structure(lattice,spcs,crdnts,coords_are_cartesian=True)
                    epot = atoms.epot
                    if structure.is_valid(min_d) and epot < 0 and epot/n_atom < float(ave_epa)/27.2114 + 0.0367493:
                        all_minhopp_structs[n_atom].append(conf)
            else:
                with open(log_file, 'a') as f:
                    f.write('no poslow.yaml file is found in {}'.format(root)+'\n')

def read_trajectories(path, min_d):
    bohr2ang=0.52917721
    with open(os.path.join(bulk_structure_optimization_dir,'ave_epa.dat'), 'r') as f:
        ave_epa = f.readline().strip()
    for root, dirs, files in os.walk(path):
        if 'posinp.yaml' in files and 'poslow.yaml' in files:
            ref_struct = Structure.from_file(os.path.join(root,'posinp.vasp'))
            n_atom = len(ref_struct.sites)
            all_mde_structures = []
            for a_file in files:
                if 'mde' in a_file and a_file.endswith('bin'):
                    atoms_all_bin=bin_read(os.path.join(root,a_file))
                    for atoms in atoms_all_bin:
                        atoms.units_length_io='angstrom'
                        atoms.cellvec[0][0]=atoms.cellvec[0][0]*bohr2ang
                        atoms.cellvec[0][1]=atoms.cellvec[0][1]*bohr2ang
                        atoms.cellvec[0][2]=atoms.cellvec[0][2]*bohr2ang
                        atoms.cellvec[1][0]=atoms.cellvec[1][0]*bohr2ang
                        atoms.cellvec[1][1]=atoms.cellvec[1][1]*bohr2ang
                        atoms.cellvec[1][2]=atoms.cellvec[1][2]*bohr2ang
                        atoms.cellvec[2][0]=atoms.cellvec[2][0]*bohr2ang
                        atoms.cellvec[2][1]=atoms.cellvec[2][1]*bohr2ang
                        atoms.cellvec[2][2]=atoms.cellvec[2][2]*bohr2ang
                        for iat in range(atoms.nat):
                            atoms.rat[iat][0]=atoms.rat[iat][0]*bohr2ang
                            atoms.rat[iat][1]=atoms.rat[iat][1]*bohr2ang
                            atoms.rat[iat][2]=atoms.rat[iat][2]*bohr2ang
                        conf=atoms2dict(atoms)
                        lattice = conf['conf']['cell']
                        crdnts = []
                        spcs = []
                        for coord in conf['conf']['coord']:
                            crdnts.append([coord[0],coord[1],coord[2]])
                            spcs.append(coord[3])
                        structure=Structure(lattice,spcs,crdnts,coords_are_cartesian=True)
                        epot = atoms.epot
                        if structure.is_valid(min_d) and epot < 0 and epot/n_atom < float(ave_epa)/27.2114 + 0.0367493:
                            all_mde_structures.append(conf)

            for i in range(0,len(all_mde_structures),20):
                all_minhopp_structs[n_atom].append(all_mde_structures[i])
