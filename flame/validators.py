import os 
from custodian.custodian import Validator
import time 

class FlameFilesValidator(Validator):
    def __init__(self):
        pass

    def check(self):
        for vfile in ["flame.out", "flame.err", "flame_log.yaml"]:
            if not os.path.exists(vfile):
                return True
        return False
