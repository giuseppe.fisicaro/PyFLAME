import os
import sys
import shutil
import re
import time
import math
import json
import yaml
from yaml import Loader 
from collections import defaultdict
import random 
from pymatgen.core.structure import Structure
from pymatgen.io.vasp import Poscar
from pymatgen.core.periodic_table import Element

from structure.io_db import get_element_list
from structure.core import min_distance
from config import *

import flame.flame_functions.atoms
from flame.flame_functions.ascii import *
from flame.flame_functions.io_yaml import *
from flame.flame_functions.vasp import *
from flame.flame_functions.latvec2dproj import *

from datetime import datetime

def write_minhocao_files(struct, step_number):
#element list
    elmnt_list = get_element_list()
#
    minhocao_temp = input_list['minhocao_temp']
    minhocao_max_temp = input_list['minhocao_max_temp']
    minhocao_n_min = input_list['minhocao_n_min']
    number_of_epoch = input_list['number_of_epoch']

#write poscur for minhocao both vasp and ascii format
    Poscar(struct).write_file('poscur.vasp')
    atoms=poscar_read('poscur.vasp')
    atoms.cellvec,atoms.rat=latvec2dproj(atoms.cellvec,atoms.rat,atoms.nat)
    ascii_write(atoms, 'poscur.ascii')

    site_symbols = []
    for site in struct:
        site_symbols.append(site.specie.symbol)
#write flame_in.yaml for minhocao
    with open('flame_in.yaml', 'a') as f:
        f.write('main:'+'\n')
        f.write('    task: minhocao'+'\n')
        f.write('    verbosity: 0'+'\n')
        f.write('    types:')
        for elmnt in elmnt_list:
            f.write(' {}'.format(elmnt))
        f.write('\n')
        f.write('    nat: {}'.format(len(struct.sites))+'\n')
        f.write('    typat:')
        for i in range(len(elmnt_list)):
            f.write(' {}*{} '.format(site_symbols.count(elmnt_list[i]),i+1))
        f.write('\n')
        f.write('    pressure: 0.0'+'\n')
        f.write('    verbose: 2'+'\n')
        f.write('    znucl: [')
        for e in range(len(elmnt_list)-1):
            f.write('{}, '.format(Element(elmnt_list[e]).Z))
        f.write('{}'.format(Element(elmnt_list[-1]).Z))
        f.write(']\n')
        f.write('    amass: [')
        for e in range(len(elmnt_list)-1):
            eam = str(Element(elmnt_list[e]).atomic_mass)
            f.write('{}, '.format(re.sub(' amu','',eam)))
        eam = str(Element(elmnt_list[-1]).atomic_mass)
        f.write('{}'.format(re.sub(' amu','',eam)))
        f.write(']\n')
        f.write('    findsym: True'+'\n'+'\n')
        f.write('potential:'+'\n')
        f.write('    potential: ann'+'\n')
        f.write('    core_rep: True'+'\n'+'\n')

        f.write('ann:'+'\n')
        f.write('    approach: atombased'+'\n'+'\n')

        f.write('geopt:'+'\n')
        f.write('    nit: 1900'+'\n')
        f.write('    #geoext: False'+'\n')
        f.write('    method: FIRE'+'\n')
        f.write('    fmaxtol: 1.E-4'+'\n')
        f.write('    strfact: 100.0'+'\n')
        f.write('    dt_start: 5.0'+'\n')
        f.write('    dt_min: 1.0'+'\n')
        f.write('    dt_max: 140.0'+'\n'+'\n')

        f.write('dynamics:'+'\n')
        f.write('    nmd: 1000'+'\n')
        f.write('    cellmass: 2.0'+'\n')
        f.write('    dt_init: 20.0'+'\n')
        f.write('    auto_mdmin: True'+'\n')
        f.write('    auto_mddt: True'+'\n')
        f.write('    nit_per_min: 30'+'\n')
        f.write('    mdmin_min: 2'+'\n')
        f.write('    mdmin_max: 4'+'\n'+'\n')

        f.write('minhopp:'+'\n')
        f.write('    auto_soft: True'+'\n')
        f.write('    nsoften: 20'+'\n')
        f.write('    alpha_at: 1.0'+'\n')
        f.write('    alpha_lat: 1.0'+'\n'+'\n')

        f.write('fingerprint:'+'\n')
        f.write('    method: OGANOV'+'\n')
        f.write('    rcut: 15.0'+'\n')
        f.write('    #dbin: 0.06'+'\n')
        f.write('    #sigma: 0.04'+'\n')
#write ioput for minhocao
    s_n = int(re.split('-',step_number)[1]) - 1
    with open('ioput', 'w') as f:
       f.write('  0.01        {}       {}         ediff, temperature, maximal temperature'.format(minhocao_temp[s_n], minhocao_max_temp[s_n])+'\n')
#write earr.dat for minhocao
    with open('earr.dat', 'w') as f:
        f.write('  0         {}          # No. of minima already found, no. of minima to be found in consecutive run'.format(minhocao_n_min[s_n])+'\n')
        f.write('  0.400000E-03  0.150000E+00  # delta_enthalpy, delta_fingerprint')
#cp ann files from train directory
    number_of_epoch = input_list['number_of_epoch']
    for root, dirs, files in os.walk(os.path.join(Flame_dir,step_number,'train',)):
        for f in files:
            for elmnt in elmnt_list:
                if f == str(elmnt)+'.ann.param.yaml.'+str(number_of_epoch).zfill(5):
                    shutil.copyfile(os.path.join(root,str(elmnt)+'.ann.param.yaml.'+str(number_of_epoch).zfill(5)),\
                                    os.path.join('./',str(elmnt)+'.ann.param.yaml'))

def store_minhocao_results(step_number):
    min_d = min_distance() * (1.1-(int(re.split('-',step_number)[1])/10))
    if (1.1-(int(re.split('-',step_number)[1])/10)) < 0.5:
        min_d = min_distance() * 0.5
    with open(os.path.join(bulk_structure_optimization_dir,'ave_epa.dat'), 'r') as f:
        ave_epa = float(f.readline().strip())
    global all_minhocao_structs 
    all_minhocao_structs = defaultdict(list) 
    read_poslows(os.path.join(Flame_dir,step_number,'minhocao'), min_d)
    read_folders('T', os.path.join(Flame_dir,step_number,'minhocao'), min_d)
    read_folders('F', os.path.join(Flame_dir,step_number,'minhocao'), min_d)
    read_folders('S', os.path.join(Flame_dir,step_number,'minhocao'), min_d)
# store data
    todump = defaultdict(list)
    for keys in all_minhocao_structs.keys():
        for values in all_minhocao_structs[keys]:
            if len(todump[keys]) < 10000:
                 todump[keys].append(values)
            else:
                break
    with open(os.path.join(Flame_dir,step_number,'minhocao','minhocao-'+step_number+'.json'), 'w') as f:
        json.dump(todump, f)

def read_poslows(path, min_d):
    nat_list = [] 
    for root, dirs in walklevel(path, 2):
        for dirname in dirs:
            if os.path.exists(os.path.join(root,dirname,'poscur.ascii')):
                if os.path.exists(os.path.join(root,dirname,'global.mon')):
                    ref_struct = Structure.from_file(os.path.join(root,dirname,'poscur.vasp'))
                    n_atom = len(ref_struct.sites)
                    if n_atom not in nat_list:
                       all_minhocao_structs[n_atom] = []
                       nat_list.append(n_atom)
                    for a_file in os.listdir(os.path.join(root,dirname)):
                        if 'poslow' in a_file and a_file.endswith('ascii'):
                            atoms=ascii_read(os.path.join(root,dirname,a_file))
                            atoms.units_length_io='angstrom'
                            conf=atoms2dict(atoms)
                            lattice = conf['conf']['cell']
                            crdnts = []
                            spcs = []
                            for coord in conf['conf']['coord']:
                                crdnts.append([coord[0],coord[1],coord[2]])
                                spcs.append(coord[3])
                                structure = Structure(lattice,spcs,crdnts,coords_are_cartesian=True)
                                epot = atoms.epot
                            if structure.is_valid(min_d) and structure.lattice.a < ref_struct.lattice.a * 2\
                                                         and structure.lattice.b < ref_struct.lattice.b * 2\
                                                         and structure.lattice.c < ref_struct.lattice.c * 2\
                                                         and epot < 0:
                                all_minhocao_structs[n_atom].append(conf)
                else:
                    with open(log_file, 'a') as f:
                        f.write('WARNING: no global.mon file is found in {}'.format(root,dirname)+'\n')
def read_folders(foldertitle, path, min_d):
    for root, dirs in walklevel(path, 2):
        for dirname in dirs:
            folder_paths = []
            if os.path.exists(os.path.join(root,dirname,'poscur.ascii')) and os.path.exists(os.path.join(root,dirname,'global.mon')):
                ref_struct = Structure.from_file(os.path.join(root,dirname,'poscur.vasp'))
                n_atom = len(ref_struct.sites)
                with open(os.path.join(root,dirname,'global.mon'), 'r') as f:
                    lines = f.readlines()[1:]
                for l in lines:
                    f_i = str(re.split('\s+',l)[1]).zfill(5)
                    if re.split('\s+',l)[11] == foldertitle:
                        folder_paths.append(os.path.join(root,dirname,'data_hop_'+f_i))
                if len(folder_paths) > 50:
                   toremove = random.sample(folder_paths, len(folder_paths)-50)
                   for tr in toremove:
                       folder_paths.remove(tr)
                posmd_file_path = []
                for folderpath in folder_paths:
                    for a_file in os.listdir(folderpath):
                        if 'posmd' in a_file and a_file.endswith('ascii'):
                            posmd_file_path.append(os.path.join(folderpath,a_file))
                steps = 20
                if len(folder_paths) > 20:
                    steps = 50
                for pfp in range(0,len(posmd_file_path),steps):
                    atoms=ascii_read(posmd_file_path[pfp])
                    atoms.units_length_io='angstrom'
                    conf=atoms2dict(atoms)
                    lattice = conf['conf']['cell']
                    crdnts = []
                    spcs = []
                    for coord in conf['conf']['coord']:
                        crdnts.append([coord[0],coord[1],coord[2]])
                        spcs.append(coord[3])
                    structure=Structure(lattice, spcs, crdnts, coords_are_cartesian=True)
                    epot = atoms.epot
                    if structure.is_valid(min_d) and structure.lattice.a < ref_struct.lattice.a * 2\
                                                 and structure.lattice.b < ref_struct.lattice.b * 2\
                                                 and structure.lattice.c < ref_struct.lattice.c * 2\
                                                 and epot < 0:
                        all_minhocao_structs[n_atom].append(conf)
                else:
                    continue
                break

def walklevel(path, depth):
    base_depth = path.rstrip(os.path.sep).count(os.path.sep)
    for root, dirs, files in os.walk(path):
        yield root, dirs
        cur_depth = root.count(os.path.sep)
        if base_depth + depth <= cur_depth:
            del dirs[:]
 
