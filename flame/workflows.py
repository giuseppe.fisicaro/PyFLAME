import os 
import sys
import json 
import re 
from collections import defaultdict

from pymatgen.core.structure import Structure
from fireworks import Workflow

from flame.fireworks import AverDistFW, TrainFW, MinhocaoFW, MinhocaoStoreFW, MinhoppFW, MinhoppStoreFW, DivCheckbFW, DivCheckcFW
from config import *

__author__ = "Sai Ram Kuchana and Hossein Mirhosseini"
__maintainer__ = "Hossein Mirhosseini"
__email__ = "mirhosse@mail.uni-paderborn.de"

def get_aver_dist_wf():
    wf_name = 'aver_dist'
    all_structs = []
    file_name = os.path.join(bulk_structure_optimization_dir,'final_relaxed_bulk_structures.json')
    with open(file_name, 'r') as f:
        all_structs  = json.loads(f.read())
    aver_dist_fws = []
    for struct_number, struct in enumerate(all_structs):
        averdistfw = AverDistFW(structure = Structure.from_dict(struct), name = 'struct-'+str(struct_number+1))
        averdistfw.spec["_category"] = 'aver_dist'
        aver_dist_fws.append(averdistfw)
    aver_dist_wf = Workflow(aver_dist_fws, name = wf_name)
    return aver_dist_wf 

def get_train_wf(c_step_number):
    wf_name = 'flame_train_'+str(c_step_number)
    train_fw = []
    trainfw = TrainFW(step_number = c_step_number, name = 'flame-train')
    trainfw.spec["_category"] = 'train'
    train_fw.append(trainfw)
    train_wf = Workflow(train_fw, name = wf_name)
    return train_wf 

def get_minhocao_wf(c_step_number):
    wf_name = 'flame_minhocao_'+str(c_step_number)
    all_structs = []
# read bulk structures 
    file_name = os.path.join(bulk_structure_optimization_dir,'final_relaxed_bulk_structures.json')
    with open(file_name, 'r') as f:
        all_structs  = json.loads(f.read())
# read poslow structures from the previos steps
    for i in range(1, int(re.split('-',c_step_number)[1])):
        step_n = 'step-'+str(i)
        f_path = os.path.join(Flame_dir,step_n,'minhocao')
        if os.path.exists(os.path.join(f_path,'poslow_structures.json')):
            with open(os.path.join(f_path,'poslow_structures.json'), 'r') as f:
                poslow_s = json.loads(f.read())
            for s in poslow_s:
                all_structs.append(Structure.from_dict(s))
    minhocao_fws = []
    for struct_number, struct in enumerate(all_structs):
        minhocaofw = MinhocaoFW(structure = struct, step_number = c_step_number, name = 'struct-'+str(struct_number+1))
        minhocaofw.spec["_category"] = 'minhocao'
        minhocao_fws.append(minhocaofw)
    minhocao_wf = Workflow(minhocao_fws, name = wf_name)
    return minhocao_wf 

def get_minhocao_store_wf(c_step_number):
    wf_name = 'flame_minhocao-store_'+str(c_step_number)
    minhocao_store_fw = []
    minhocaostorefw = MinhocaoStoreFW(step_number = c_step_number, name = 'minhocao_store_results')
    minhocaostorefw.spec["_category"] = 'minhocao'
    minhocao_store_fw.append(minhocaostorefw)
    minhocao_store_wf = Workflow(minhocao_store_fw, name = wf_name)
    return minhocao_store_wf

def get_minhopp_wf(c_step_number):
    wf_name = 'flame_minhopp_'+str(c_step_number)
    all_clusters = []
    file_name = os.path.join(bulk_structure_optimization_dir,'cluster_structures.json')
    with open(file_name, 'r') as f:
        all_clusters  = json.loads(f.read())
    minhopp_fws = []
    for struct_number, struct in enumerate(all_clusters):
        minhoppfw = MinhoppFW(structure = struct, step_number = c_step_number, name = 'struct-'+str(struct_number+1))
        minhoppfw.spec["_category"] = 'minhopp'
        minhopp_fws.append(minhoppfw)
    minhopp_wf = Workflow(minhopp_fws, name = wf_name)
    return minhopp_wf

def get_minhopp_store_wf(c_step_number):
    wf_name = 'flame_minhopp-store_'+str(c_step_number)
    minhopp_store_fw = []
    minhoppstorefw = MinhoppStoreFW(step_number = c_step_number, name = 'minhopp_store_results')
    minhoppstorefw.spec["_category"] = 'minhopp'
    minhopp_store_fw.append(minhoppstorefw)
    minhopp_store_wf = Workflow(minhopp_store_fw, name = wf_name)
    return minhopp_store_wf

def get_divcheck_b_wf(c_step_number):
    wf_name = 'flame_divcheck-bulk_'+str(c_step_number)
    with open(os.path.join(Flame_dir,c_step_number,'minhocao','minhocao-'+c_step_number+'.json'), 'r') as f:
        minhocao_confs = json.loads(f.read())
    divcheck_b_fws = []
    for natom in minhocao_confs.keys():
        divcheckbfw = DivCheckbFW(number_of_atom = natom, step_number = c_step_number, name = str(natom)+'-atoms-bulk')
        divcheckbfw.spec["_category"] = 'divcheck-bulk'
        divcheck_b_fws.append(divcheckbfw)
    divcheck_b_wf = Workflow(divcheck_b_fws, name = wf_name)
    return divcheck_b_wf

def get_divcheck_c_wf(c_step_number):
    wf_name = 'flame_divcheck-cluster_'+str(c_step_number)
    with open(os.path.join(Flame_dir,c_step_number,'minhopp','minhopp-'+c_step_number+'.json'), 'r') as f:
        minhopp_confs = json.loads(f.read())
    divcheck_c_fws = []
    for natom in minhopp_confs.keys():
        divcheckcfw = DivCheckcFW(number_of_atom = natom, step_number = c_step_number, name = str(natom)+'-atoms-cluster')
        divcheckcfw.spec["_category"] = 'divcheck-cluster'
        divcheck_c_fws.append(divcheckcfw)
    divcheck_c_wf = Workflow(divcheck_c_fws, name = wf_name)
    return divcheck_c_wf

