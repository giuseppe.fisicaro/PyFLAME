import os 
from fireworks import Workflow
from custodian.vasp.handlers import VaspErrorHandler, MeshSymmetryErrorHandler, UnconvergedErrorHandler, NonConvergingErrorHandler, PositiveEnergyErrorHandler, FrozenJobErrorHandler, StdErrHandler, DriftErrorHandler, PotimErrorHandler
from atomate.vasp.fireworks.core import OptimizeFW
from atomate.vasp.powerups import use_custodian
from pymatgen.core.structure import Structure
from pymatgen.io.vasp.sets import MITRelaxSet
from config import *

def get_vasp_wf(structs, calculation_type, wf_name):

    handler_groups = [VaspErrorHandler(), MeshSymmetryErrorHandler(), UnconvergedErrorHandler(), NonConvergingErrorHandler(), FrozenJobErrorHandler(timeout=600), StdErrHandler()]
#
    with open('./VASP/vasp_files/INCAR.yaml', 'r') as f:
        uis = yaml.load(f, Loader=yaml.FullLoader)
    user_incar_settings = uis['default']
    if calculation_type == 'bulk':
        user_incar_settings['EDIFFG'] = uis['bulk']['EDIFFG']
        user_incar_settings['ISIF']   = uis['bulk']['ISIF']
        user_incar_settings['NSW']    = uis['bulk']['NSW']
    if calculation_type == 'perturbed-bulk':
        user_incar_settings['EDIFFG'] = uis['perturbed-bulk']['EDIFFG']
        user_incar_settings['ISIF']   = uis['perturbed-bulk']['ISIF']
        user_incar_settings['NSW']    = uis['perturbed-bulk']['NSW']
    if calculation_type == 'NSW0-bulk':
        user_incar_settings['EDIFFG'] = uis['NSW0-bulk']['EDIFFG']
        user_incar_settings['ISIF']   = uis['NSW0-bulk']['ISIF']
        user_incar_settings['NSW']    = uis['NSW0-bulk']['NSW']
    if calculation_type == 'NSW0-cluster':
        user_incar_settings['EDIFFG'] = uis['NSW0-cluster']['EDIFFG']
        user_incar_settings['ISIF']   = uis['NSW0-cluster']['ISIF']
        user_incar_settings['NSW']    = uis['NSW0-cluster']['NSW']
#
    with open('./VASP/vasp_files/KPOINTS.yaml', 'r') as f:
        user_kpoints_settings = yaml.load(f, Loader=yaml.FullLoader)
    if not user_kpoints_settings['reciprocal_density']:
        user_kpoints_settings = None
#
    vasp_fws = []
    for s in structs:
        struct = Structure.from_dict(s)
        vis_relax = MITRelaxSet(struct, user_incar_settings = user_incar_settings)
        v = vis_relax.as_dict()
        v.update({"user_kpoints_settings": user_kpoints_settings})
        vis_relax = vis_relax.__class__.from_dict(v)
        fw_name = struct.composition.reduced_formula
        optimizefw = OptimizeFW(structure = struct, vasp_input_set = vis_relax, name = fw_name, db_file = False)
        optimizefw.spec["_category"] = calculation_type
        vasp_fws.append(optimizefw)
    wf0 = Workflow(vasp_fws, name = wf_name, metadata = None)
    wf = use_custodian(wf0, custodian_params = {"handler_group": handler_groups, "max_errors": 5})
    return wf 
