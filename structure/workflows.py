import os
import sys
from fireworks import Workflow

from structure.fireworks import *
from structure.cryspnet_csp import get_space_groups

from config import *

def get_step1_wf(original_species_list, composition_list):
    step1_fws = []
# Ionic-subctitution crystal scructure prediction
    if 1 in input_list['crystal_structure_prediction_method']:
        if len(original_species_list) > 0: 
#            with open(log_file, 'a') as f:
#                f.write('Ionic-subctitution crystal structure prediction'+'\n')
# i_s_csp_fw
            iscpsfw = ISCSPFW(name = 'i_s_csp', original_species = original_species_list)
            iscpsfw.spec["_priority"] = 4
            iscpsfw.spec["_category"] = 'csp'

            step1_fws.append(iscpsfw)
        else:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: it is not possible to perform ionic-subctitution crystal structure prediction. \
                             Oxidations states are not known. <<<'+'\n')
            exit()
# CRYSPNet crystal structure prediction
    if 2 in input_list['crystal_structure_prediction_method']:
        if len(composition_list) > 0:
#            with open(log_file, 'a') as f:
#                f.write('CRYSPNet crystal structure prediction'+'\n')
#
            sg_dict = get_space_groups(composition_list, 14, 50, bravais_prob_cutoff = input_list['threshold'], spacegroup_prob_cutoff = input_list['threshold'])
            formula_list = []
            space_group_list = []
            for frml, sgs in sg_dict.items():
                formula_list.append(frml)
                space_group_list.append(sgs)
# cryspnet_csp_fw
            cryspnetcspfw = CryspnetCSPFW(name = 'cryspnet_csp', formulas = formula_list, space_groups = space_group_list)
            cryspnetcspfw.spec["_priority"] = 3
            cryspnetcspfw.spec["_category"] = 'csp'
            step1_fws.append(cryspnetcspfw)
        else:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: it is not possible to perform CRYSPNet crystal structure prediction. \
                             No chemical formula is provided. <<<'+'\n')
            exit()
# random structure generation
    if 0 in input_list['crystal_structure_prediction_method']:
        if len(composition_list) > 0:
# random_struct_fw
            randombulkstructfw = RandomBulkStructFW(name = 'random_bulk', compositions = composition_list)
            randombulkstructfw.spec["_priority"] = 2
            randombulkstructfw.spec["_category"] = 'csp'
            step1_fws.append(randombulkstructfw)
        else:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: it is not possible to perform PyXtal random structure generation. \
                             No chemical formula is provided. <<<'+'\n')
            exit()
    if len(step1_fws) == 0:
        with open(log_file, 'a') as f:
            f.write('WARNING: neither crystal structure prediction nor random structure generation is performed'+'\n')
# clusters
    if input_list['cluster_calculation']:
        if len(composition_list) > 0:

# random_cluster_fw
            randomclusterstructfw = RandomClusterStructFW(name = 'random_cluster', compositions = composition_list)
            randomclusterstructfw.spec["_priority"] = 1
            randomclusterstructfw.spec["_category"] = 'csp'
            step1_fws.append(randomclusterstructfw)
        else:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: it is not possible to perform PyXtal random structure generation. \
                             No chemical formula is provided. <<<'+'\n')
            exit()
    return Workflow(step1_fws, name = 'step-1')
