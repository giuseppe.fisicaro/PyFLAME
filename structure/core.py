import os 
import sys
import yaml
import json
from config import *
import numpy as np

from pymatgen.core.structure import Structure
from pymatgen.analysis.molecule_structure_comparator import CovalentRadius

def min_distance():
    elmnt_list = []
    covalent_radius = CovalentRadius.radius
    with open(os.path.join(bulk_structure_optimization_dir,'elements.dat'), 'r') as f:
        elmnt_list = [line.strip() for line in f]
    elmnt_c_radius_list = []
    for elmnt in elmnt_list:
        elmnt_c_radius_list.append(covalent_radius[elmnt])
    elmnt_radii_arr = np.array(elmnt_c_radius_list).astype(np.float)
    min_distance = elmnt_radii_arr.min() * 2.0 * input_list['min_distance_prefactor']
    return min_distance

def r_cut():
    all_structs = []
    all_rcuts = []
    F_n_atom_in_rcut = input_list['n_atom_in_rcut']
    file_name =  os.path.join(bulk_structure_optimization_dir,'final_relaxed_bulk_structures.json')
    with open(file_name, 'r') as f:
        f_s=json.loads(f.read())
    for s in f_s:
        all_structs.append(s)
    for struct in all_structs:
        for d in range(1,15):
            if len(Structure.from_dict(struct).get_sites_in_sphere([0,0,0],d)) < F_n_atom_in_rcut:
                continue
            else:
                all_rcuts.append(d)
                break
    return sum(all_rcuts)/len(all_rcuts) 
