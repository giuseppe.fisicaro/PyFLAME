import os
import yaml
from time import sleep

from config import *
from fireworks.core.fworker import FWorker
from fireworks.core.launchpad import LaunchPad
from fireworks.queue.queue_launcher import rapidfire
from fireworks.utilities.fw_serializers import load_object_from_file


with open(os.path.join(CONFIG_DIR,'my_launchpad.yaml'),'r') as f:
    lpd = yaml.full_load(f)
l_host = lpd['host']
l_port = lpd['port']
l_name = lpd['name']
l_username = lpd['username']
l_password = lpd['password']
launchpad = LaunchPad(host = l_host, port = l_port, name = l_name, username = l_username, password = l_password, ssl = True, authsource = 'admin')

def launchpad_reset():
    try:
        launchpad.reset('', require_password = False, max_reset_wo_password = 10000)
        return True
    except:
        return False

def add_wf(wf):
    launchpad.add_wf(wf)

def run_jobs(job_type, l_dir):
# setup qadapter and fworker
    qadapter = load_object_from_file(os.path.join(CONFIG_DIR,'my_qadapter.yaml'))
    with open(os.path.join(CONFIG_DIR,'my_fworker.yaml'),'r') as f:
        fworker_dict = yaml.load(f, Loader=yaml.FullLoader)

    with open('./workflows/job_script/job_script.yaml', 'r') as f:
        js = yaml.load(f, Loader=yaml.FullLoader)
    with open('./workflows/job_script/list_of_jobtypes.yaml', 'r') as f:
        ljts = yaml.load(f, Loader=yaml.FullLoader)
    job_type_list = ljts['job_type_list']

    if job_type in job_type_list:
# fworker runs only this job type
        fworker_dict['category'] = [job_type]
# number of jobs
        nqueue = js[job_type]['number_of_jobs']
# time and number of cores and nodes 
        if js[job_type]['number_of_node']:
            qadapter['nodes'] = js[job_type]['number_of_node']
        if js[job_type]['number_of_cpu']:
            qadapter['ncpu']  = js[job_type]['number_of_cpu']
        if js[job_type]['time']:
            qadapter['time']  = js[job_type]['time']
    else:
        return False
#
    with open(os.path.join(CONFIG_DIR,'my_fworker.yaml'),'w') as f:
       yaml.dump(fworker_dict, f)
#
#run in rapidfire mode
    fworker = FWorker.from_file(os.path.join(CONFIG_DIR,'my_fworker.yaml')) 
    rapidfire(launchpad, fworker, qadapter, launch_dir=l_dir, njobs_queue=nqueue, nlaunches=0, reserve=False)
#
    fworker_dict['category'] =  '__none__'
    with open(os.path.join(CONFIG_DIR,'my_fworker.yaml'),'w') as f:
       yaml.dump(fworker_dict, f)
#
    return True

def run_exists():
    state = False
    if launchpad.future_run_exists() or launchpad.get_fw_ids(query={'state': 'RUNNING'}):
        state = True
    return state

def check_lp(wfname_list):
    for wfname in wfname_list:
        try:
            wfid = launchpad.get_wf_ids({"name": wfname})
            wf_summary = launchpad.get_wf_summary_dict(wfid[0],mode="less")
        except:
            return 'unknown'
        if wf_summary['state'] == 'FIZZLED':
             return wf_summary['state']
    return wf_summary['state']

def fizzle_lostruns():
    launchpad.detect_lostruns(expiration_secs=600, fizzle=False, refresh = True)

def rerun():
    fwids = []
    fwids = launchpad.get_fw_ids(query={"state":"READY"})
    if fwids: 
        wfid =  launchpad.get_wf_by_fw_id(fwids[0])
        if wfid:
            wf_name = wfid.name
            if wf_name == 'step-2':
                run_jobs('bulk', bulk_structure_optimization_dir)
            elif 'step-5_perturbed' in wf_name or 'step-5_additional' in wf_name:
                run_jobs('perturbed-bulk', perturbed_bulk_structure_optimization_dir)
            elif 'flame' in wf_name or 'VASP' in wf_name:
                stpnm = wf_name.split('_')[1]
                stpnmbr = wf_name.split('_')[-1]
                if wf_name.split('_')[0] == 'flame':
                    if 'divcheck' in stpnm:
                        run_jobs(stpnm, os.path.join(Flame_dir,stpnmbr,stpnm.split('-')[0], stpnm.split('-')[1]))
                    else:
                        run_jobs(stpnm, os.path.join(Flame_dir,stpnmbr,stpnm))
                if wf_name.split('_')[0] == 'VASP':
                    if 'bulk' in wf_name.split('_')[2] or 'stressed' in wf_name.split('_')[2]:
                        run_jobs('NSW0-bulk', os.path.join(Flame_dir,stpnmbr,'VASP_run','bulk'))
                    if 'cluster' in wf_name.split('_')[2]:
                        run_jobs('NSW0-cluster', os.path.join(Flame_dir,stpnmbr,'VASP_run','cluster'))
            else:
                return False
    else:
        return False
