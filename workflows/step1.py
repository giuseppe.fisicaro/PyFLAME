import os
import sys
import yaml
import json
from sys import exit
from datetime import datetime 
from cryspnet.utils import FeatureGenerator, topkacc
from cryspnet.config import *
from config import *
from structure.io_db import *
from structure.workflows import *  
from structure.user_ion_substitution import user_ion_substitution
from workflows.core import *

def step_1():
    with open(log_file, 'a') as f:
        f.write('STEP 1'+'\n')
        f.write('Start time: {}'.format(datetime.now())+'\n')
        f.write('For more details see {}/bulk_structure.dat'.format(bulk_structure_optimization_dir)+'\n')
# create folders
    try:
        os.mkdir(bulk_structure_optimization_dir)
    except FileExistsError:
        with open(log_file, 'a') as f:
            f.write(">>> Cannot proceed. {} exists. <<".format(bulk_structure_optimization_dir)+'\n')
        exit()
# read the original composition
    original_species = []
    elment_list = []
    composition = []
    original_species, element_list = get_original_species()
    if input_list['Composition'][0]['MP_ID'] or input_list['Composition'][2]['chemical_formula']:   
        compositions = get_original_composition()
# write elements in elements.dat 
    write_elements(element_list)
# reset LPAD
    if run_exists():
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
        exit()
    elif not launchpad_reset():
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: cannot reset Launchpad <<<'+'\n')
        exit()
    else:
        with open(log_file, 'a') as f:
                f.write('Reseting Launchpad'+'\n')
# get workflow
    step1_wf = get_step1_wf(original_species, compositions)
# add to lp
    add_wf(step1_wf)
# run csp/random structure generation jobs
    if not run_jobs('csp', bulk_structure_optimization_dir):
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed <<<'+'\n')
        exit()
# wait until all jobs are done
    while True:
        if run_exists():
            fizzle_lostruns()
            sleep(60)
        else:
           break
# check launchpad state
    lp_state = check_lp(['step-1'])
    if lp_state == 'FIZZLED':
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: csp/random structure workflow is FIZZLED <<<'+'\n')
    if lp_state == 'unknown':
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed. The state of Workflow is not known.<<<'+'\n')
        exit()
# store all bulk structures in one file
    all_bulk_structures = []
    fpath = os.path.join(bulk_structure_optimization_dir,'ionic_substitution_csp_structures.json')
    if os.path.exists(fpath):
        filesize = os.path.getsize(fpath)
        if not filesize == 0:
            with open(fpath,'r') as f:
                i_s_csp_structs = json.loads(f.read())
            f.close()
            all_bulk_structures.extend(i_s_csp_structs)
    fpath = os.path.join(bulk_structure_optimization_dir,'cryspnet_csp_structures.json')
    if os.path.exists(fpath):
        filesize = os.path.getsize(fpath)
        if not filesize == 0:
            with open(fpath,'r') as f:
                c_csp_structs = json.loads(f.read())
            f.close()
            all_bulk_structures.extend(c_csp_structs)
    fpath = os.path.join(bulk_structure_optimization_dir,'random_structures.json')
    if os.path.exists(fpath):
        filesize = os.path.getsize(fpath)
        if not filesize == 0:
            with open(fpath,'r') as f:
                r_structs = json.loads(f.read())
            f.close()
            all_bulk_structures.extend(r_structs)
    if len(all_bulk_structures) == 0:
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed. No bulk structure for optimixation <<<'+'\n')
        exit()
    else:
        with open(os.path.join(bulk_structure_optimization_dir,'all_bulk_structures.json'),'w') as f:
            json.dump(all_bulk_structures, f)
# user ion substitution
    if input_list['ionic_substitution']:
        with open(log_file, 'a') as f:
            f.write('User ion substitution'+'\n')
        from structure.user_ion_substitution import user_ion_substitution
        user_ion_substitution()
# 
    with open(log_file, 'a') as f:
        f.write('STEP 1 ended'+'\n')
        f.write('End time: {}'.format(datetime.now())+'\n')
    return steps_status[1]
