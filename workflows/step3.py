import os
import sys
import json
import yaml
import shutil
from datetime import datetime
from pymatgen.core.structure import Structure
from pymatgen.analysis.structure_matcher import StructureMatcher, ElementComparator
from config import *
from flame.core import write_p_f_from_list

def step_3():
    with open(log_file, 'a') as f:
        f.write("---------------------------------------------------------------------------------------------------"+'\n')
        f.write('STEP 3'+'\n')
        f.write('Start time: {}'.format(datetime.now())+'\n')  
# remove duplicate structuree
    relaxed_bulk_structs_dict = []
    nonduplicated_structs_dict = [] # list of non duplicated structures
    nonduplicated_structs_epot = [] # energy for non duplicated structures 
    nonduplicated_structs_epa = [] # energy per atom for non duplicated structures 
    seen_structs = [] # list of all seen structures, independent of chemical system
    seenstruct = {}
    sm = StructureMatcher(comparator=ElementComparator(),primitive_cell=True)
    for root, dirs, files in os.walk(bulk_structure_optimization_dir):
        if 'task.json' in files:
            with open(os.path.join(root,'task.json'), 'r') as f:
                s = json.loads(f.read())
            relaxed_bulk_structs_dict.append(s['output']['structure'])
            found = False
            for seenstruct in seen_structs:
                if sm.fit(Structure.from_dict(s['output']['structure']), Structure.from_dict(seenstruct)):
                    found = True
                    break
            if not found:
                nonduplicated_structs_dict.append(s['output']['structure'])
                nonduplicated_structs_epot.append(s['output']['energy'])
                nonduplicated_structs_epa.append(s['output']['energy_per_atom'])
                seen_structs.append(s['output']['structure'])
                
# remove too high and too low energy structures
    final_structs_dict = [] 
    min_epa = min(nonduplicated_structs_epa)
    max_epa = max(nonduplicated_structs_epa)
    ave_epa = sum(nonduplicated_structs_epa)/len(nonduplicated_structs_epa)
    forplot = []
    structure_list = []
    epot_list = []
    for i in range(len(nonduplicated_structs_dict)):
        if nonduplicated_structs_epa[i] < 0\
        and nonduplicated_structs_epa[i] < ave_epa + 1.0\
        and nonduplicated_structs_epa[i] > ave_epa - 2.0:
            forplot.append([len(Structure.from_dict(nonduplicated_structs_dict[i]).sites), nonduplicated_structs_epa[i]]) 
            final_structs_dict.append(nonduplicated_structs_dict[i])
            structure_list.append(nonduplicated_structs_dict[i])
            epot_list.append(nonduplicated_structs_epa[i])
 
    write_p_f_from_list(structure_list, 'bulk', epot_list, False, os.path.join(bulk_structure_optimization_dir,'final_relaxed_bulk_structures.yaml'))
# store minimum energy/atom
    with open(os.path.join(bulk_structure_optimization_dir,'minmax_epa.dat'), 'w') as f:
        f.write(str(min_epa)+'\n')
        f.write(str(max_epa)+'\n')
# store average energy/atom
    with open(os.path.join(bulk_structure_optimization_dir,'ave_epa.dat'), 'w') as f:
        f.write(str(ave_epa))
# 
    with open(os.path.join(bulk_structure_optimization_dir,'nat_epa.dat'), 'w') as f:
        for i in range(len(forplot)):
            f.write('{} {}'.format(str(forplot[i][0]), str(forplot[i][1]))+'\n')
# store bulk structures
    with open(os.path.join(bulk_structure_optimization_dir,'final_relaxed_bulk_structures.json'), 'w') as f:
        json.dump(final_structs_dict, f)
# number of finished jobs
    with open(os.path.join(bulk_structure_optimization_dir,'bulk_structure.dat'), 'a') as f:
        f.write("number of finished jobs: {}".format(len(relaxed_bulk_structs_dict))+'\n')

    with open(os.path.join(bulk_structure_optimization_dir,'bulk_structure.dat'), 'a') as f:
        f.write("Number of relaxed bulk structures after removing duplicated and high energy structures: {}".format(len(final_structs_dict))+'\n')

    with open(log_file, 'a') as f:
        f.write('STEP 3 ended.'+'\n')
        f.write('End time: {}'.format(datetime.now())+'\n')
    return steps_status[3] 

