import os
import sys
from datetime import datetime
from sys import exit
from config import *
from structure.perturb import create_perturbed_structures

def step_4():
    with open(log_file, 'a') as f:
        f.write("---------------------------------------------------------------------------------------------------"+'\n')
        f.write('STEP 4'+'\n')
        f.write('Start time: {}'.format(datetime.now())+'\n')
        f.write('For more details see {}/perturbed-bulk_stressed-structure.dat'.format(perturbed_bulk_structure_optimization_dir)+'\n')
# create directories 
    try:
        os.mkdir(perturbed_bulk_structure_optimization_dir)
    except FileExistsError:
        with open(log_file, 'a') as f:
            f.write(">>> Cannot proceed. {} directory exists. <<<".format(perturbed_bulk_structure_optimization_dir)+'\n')
        exit()
#
    create_perturbed_structures()
#
    with open(log_file, 'a') as f:
        f.write('STEP 4 is done'+'\n')
        f.write('End time: {}'.format(datetime.now())+'\n')
    return steps_status[4] 
